# Dependencies

Check `requirements.txt` for application dependencies. Some dependencies need compilation, so be sure to fulfil system level dependencies.

Check Ansible module pip for installing application dependencies.

# Setup Virtualenv

`virtualenv --no-site-packages .venv`

# Install Requirements (requirements.txt)

`pip install -r requirements.txt`

# Considerations

* Do not forget to copy rendered production files to target hosts
* Use `.env` as running configuration and `.env.template` as baseline
* Use lookups for password generation
* Save secrets in a secure manner using [Pass (Password Store)](https://www.passwordstore.org) or Ansible Vault
* Read about Ansible shell module in order to change working directory and use skpi conditions
* Use Ansible register attribute in order to save data into a variable from a previous task
