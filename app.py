from flask import (
    Flask,
    render_template,
    redirect,
    request
)
from passlib.hash import sha512_crypt
import random
import string
import records

app = Flask(__name__)
app.config.from_pyfile('.env')

db = records.Database(app.config['DB_URL'])


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html', rows=rows())


@app.route('/', methods=['POST'])
def store():
    length = int(request.form['length'])
    password = generate_password(length)
    password_hash = generate_hash(password)
    insert(password, password_hash)
    return redirect('/')


@app.route('/<int:id>', methods=['GET'])
def destroy(id):
    delete(id)
    return redirect('/')


def generate_password(length):
    chars = ''.join((
        string.ascii_lowercase,
        string.ascii_uppercase,
        string.digits,
        string.punctuation
    ))
    return ''.join(random.SystemRandom().choice(chars) for _ in range(length))


def generate_hash(password):
    return sha512_crypt.using(rounds=12000).hash(password)


def insert(password, password_hash):
    db.query(app.config['INSERT'], password=password, password_hash=password_hash)


def delete(id):
    db.query(app.config['DELETE'], id=id)


def rows():
    return db.query(app.config['SELECT'])

if __name__ == "__main__":
    app.run(debug=app.config['DEBUG'], use_reloader=app.config['DEBUG'], host='0.0.0.0')
